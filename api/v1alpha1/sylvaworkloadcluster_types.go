/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	apiextensionsv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	OpenstackInfrastructureProvider = "openstack"
	VsphereInfrastructureProvider   = "vsphere"
	DockerInfrastructureProvider    = "docker"
	Metal3InfrastructureProvider    = "metal3"

	Rke2BootstrapProvider    = "rke2"
	KubeadmBootstrapProvider = "kubeadm"
)

// SylvaWorkloadClusterSpec defines the desired state of SylvaWorkloadCluster
type SylvaWorkloadClusterSpec struct {
	// Kubernetes version for the cluster, e.g. "1.27".
	// Note that this is *not* a full version, but only the two first numbers.
	// +kubebuilder:validation:Pattern=`^[1-9]+[0-9]*[.]([1-9]+[0-9]|0)+$`
	K8sVersion string `json:"k8sVersion,omitempty"`

	// +kubebuilder:validation:Enum=openstack;docker;vsphere;metal3
	// Infrastructure specifies the underlying infrastructure provider for the cluster.
	// Supported values and their corresponding Cluster API Providers:
	// "openstack" maps to the Cluster API Provider OpenStack (capo),
	// "vsphere" maps to the Cluster API Provider vSphere (capv),
	// "docker" maps to the Cluster API Provider Docker (capd),
	// "metal3" maps to the Cluster API Provider Metal3 (capm3).
	Infrastructure string `json:"infrastructure"`

	// +required
	SylvaSourceVersion *SylvaSourceVersion `json:"sylvaSourceVersion"`

	// +required
	ControlPlane *ControlPlaneConfig `json:"controlPlane"`

	// +required
	MachineDeployments map[string]MachineDeploymentSpec `json:"machineDeployments"`

	// ClusterSettings is a free-form JSON that holds specific settings for the cluster.
	// This field will be mapped to SylvaUnitsRelease.spec.values.cluster.
	// Note well that it can't hold sylva-units templated values (strings with '{{ ... }}')
	// +optional
	ClusterSettings apiextensionsv1.JSON `json:"clusterSettings,omitempty"`

	// SecretValues define the (relative) path to a JSON secret and the key, stored in the Sylva secret storage (ex: /secret/data/${current namespace}/${SecretValues.path}/${SecretValues.key} in Vault).
	// This field will be mapped to SylvaUnitsRelease.spec.values.clusterSecrets.
	// +optional
	SecretValues SecretValuesSpec `json:"secretValues,omitempty"`

	// Defines the units to be enabled in the workload cluster.
	// Each unit is represented as a key in this object with a boolean value indicating its enabled status.
	// +optional
	EnabledUnits map[string]bool `json:"enabledUnits,omitempty"`

	// OverridingValues is a free-form JSON that will be mapped to SylvaUnitsRelease.spec.values.
	// It overrides existing values. Eventually, this field will be removed once the API is stabilized
	// Note well that it can't hold sylva-units templated values (strings with '{{ ... }}')
	// In addition, it can not override values passed through the CRD fields (ex: cluster, k8s_version_short, etc.)
	// +optional
	OverridingValues apiextensionsv1.JSON `json:"overridingValues,omitempty"`

	// Suspend tells the controller to suspend reconciliation for this SylvaUnitsRelease,
	// +kubebuilder:default=false
	// +optional
	Suspend bool `json:"suspend,omitempty"`
}

// +kubebuilder:validation:XValidation:rule="has(self.tag) || has(self.branch) || has(self.commit)",message="At least one of 'commit', 'branch', or 'tag' must be specified"
type SylvaSourceVersion struct {

	// +optional
	Tag string `json:"tag,omitempty"`

	// +optional
	Branch string `json:"branch,omitempty"`

	// +optional
	Commit string `json:"commit,omitempty"`
}

type ControlPlaneConfig struct {
	// +kubebuilder:validation:Enum=rke2;kubeadm
	// +required
	// Provider specifies how Kubernetes base components are deployed: "kubeadm" for
	// Kubeadm ("cabpk" CAPI provider), "rke2" for Rancher RKE2.
	Provider string `json:"provider"`

	// Number of control plane nodes
	// +kubebuilder:validation:Minimum=1
	// +optional
	Replicas *int32 `json:"replicas,omitempty"`
}

// MachineDeploymentSpec defines sets of cluster nodes (implemented with Cluster API Machine Deployments)
type MachineDeploymentSpec struct {
	Replicas int32 `json:"replicas"`
}

// Define the relative path / key to access to the Sylva secret storage
// +kubebuilder:validation:XValidation:rule="!(has(self.path) && self.path.matches('../'))",message="'../' is not allowed in secretValues:path"
// +kubebuilder:validation:XValidation:rule="!(has(self.key) && self.key.matches('../'))",message="'../' is not allowed in secretValues:key"
type SecretValuesSpec struct {
	Path string `json:"path,omitempty"`
	Key  string `json:"key,omitempty"`
}

// SylvaWorkloadClusterStatus defines the observed state of SylvaWorkloadCluster
type SylvaWorkloadClusterStatus struct {
	// ObservedGeneration is the last observed generation.
	// +optional
	ObservedGeneration int64 `json:"observedGeneration,omitempty"`

	// +optional
	Conditions []metav1.Condition `json:"conditions,omitempty"`
}

// GetConditions returns the status conditions of the object.
func (in SylvaWorkloadCluster) GetConditions() []metav1.Condition {
	return in.Status.Conditions
}

// SetConditions sets the status conditions on the object.
func (in *SylvaWorkloadCluster) SetConditions(conditions []metav1.Condition) {
	in.Status.Conditions = conditions
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
//+kubebuilder:resource:shortName=swc

// SylvaWorkloadCluster is the Schema for the sylvaworkloadclusters API
type SylvaWorkloadCluster struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec SylvaWorkloadClusterSpec `json:"spec,omitempty"`
	// +kubebuilder:default:={"observedGeneration":-1}
	Status SylvaWorkloadClusterStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// SylvaWorkloadClusterList contains a list of SylvaWorkloadCluster
type SylvaWorkloadClusterList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []SylvaWorkloadCluster `json:"items"`
}

func init() {
	SchemeBuilder.Register(&SylvaWorkloadCluster{}, &SylvaWorkloadClusterList{})
}
