/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// +kubebuilder:docs-gen:collapse=Apache License

/*
Ideally, we should have one `<kind>_controller_test.go` for each controller scaffolded and called in the `suite_test.go`.
So, let's write our example test for the SylvaWorkloadCluster controller (`sylvaworkloadcluster_controller_test.go.`)
*/

/*
As usual, we start with the necessary imports. We also define some utility variables.
*/
package controller

import (
	"context"
	"encoding/json"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"

	apiextensionsv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"

	surv1alpha1 "gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/api/v1alpha1"
	swcv1alpha1 "gitlab.com/sylva-projects/sylva-elements/workload-cluster-operator/api/v1alpha1"
)

// +kubebuilder:docs-gen:collapse=Imports

var (
	// Common timeouts and retry intervals
	timeout  = time.Second * 30
	interval = time.Millisecond * 500
)

// Types used to unmarshall binary values
type surTestCapo struct {
	Network_id string
	Baz        string
}
type surTestCluster struct {
	Capo surTestCapo
}
type surTestValues struct {
	Cluster           surTestCluster
	K8s_version_short string
	Foo               string
}

func createSylvaWorkloadCluster() *swcv1alpha1.SylvaWorkloadCluster {
	replica_1 := int32(1)

	sylvaWorkloadCluster := &swcv1alpha1.SylvaWorkloadCluster{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "test",
			Namespace: "default",
		},
		Spec: swcv1alpha1.SylvaWorkloadClusterSpec{
			K8sVersion:     "1.29",
			Infrastructure: swcv1alpha1.DockerInfrastructureProvider,
			ControlPlane: &swcv1alpha1.ControlPlaneConfig{
				Provider: swcv1alpha1.Rke2BootstrapProvider,
				Replicas: &replica_1,
			},
			SylvaSourceVersion: &swcv1alpha1.SylvaSourceVersion{
				Branch: "main",
			},
			MachineDeployments: map[string]swcv1alpha1.MachineDeploymentSpec{
				"md1": {
					Replicas: replica_1,
				},
			},
			SecretValues: swcv1alpha1.SecretValuesSpec{
				Path: "common",
				Key:  "capo",
			},
			ClusterSettings: apiextensionsv1.JSON{
				Raw: []byte(`{"capo":{"network_id":"abc123"}}`),
			},
		},
	}

	return sylvaWorkloadCluster
}

func getSURFromSWC(ctx context.Context, name string, namespace string) *surv1alpha1.SylvaUnitsRelease {
	//Getting the SylvaWorkloadCluster from namespaced name
	sylvaWorkloadClusterNamspacedName := types.NamespacedName{Name: name, Namespace: namespace}
	sylvaWorkloadCluster := &swcv1alpha1.SylvaWorkloadCluster{}
	Eventually(func(g Gomega) {
		g.Expect(k8sClient.Get(ctx, sylvaWorkloadClusterNamspacedName, sylvaWorkloadCluster)).To(Succeed())
	}, timeout, interval).Should(Succeed())

	// By("Waiting for SylvaWorkloadCluster to have a Ready condition set")
	Eventually(func(g Gomega) {
		g.Expect(k8sClient.Get(ctx, sylvaWorkloadClusterNamspacedName, sylvaWorkloadCluster)).To(Succeed())
		g.Expect(sylvaWorkloadCluster.Status.Conditions).To(ContainElement(
			// Just as an example as we can't have a Ready SWC as long as we don't mock the SylvaUnitRelease status
			MatchFields(IgnoreExtras, Fields{"Type": Equal("Ready")})))
	}, timeout, interval).Should(Succeed())

	// Getting the created sylva unit release
	sylvaUnitReleaseNamspacedName := types.NamespacedName{Name: sylvaWorkloadCluster.Name, Namespace: sylvaWorkloadCluster.Namespace}
	sylvaUnitRelease := &surv1alpha1.SylvaUnitsRelease{}
	Eventually(func(g Gomega) {
		g.Expect(k8sClient.Get(ctx, sylvaUnitReleaseNamspacedName, sylvaUnitRelease)).To(Succeed())
	}, timeout, interval).Should(Succeed())

	return sylvaUnitRelease
}

func removeSWC(ctx context.Context, swc *swcv1alpha1.SylvaWorkloadCluster) {
	Expect(k8sClient.Delete(ctx, swc)).Should(Succeed())
	createdSylvaWorkloadCluster := &swcv1alpha1.SylvaWorkloadCluster{}
	Eventually(func(g Gomega) {
		g.Expect(k8sClient.Get(ctx, types.NamespacedName{Name: swc.Name, Namespace: swc.Namespace}, createdSylvaWorkloadCluster)).To(Not(Succeed()))
	}, timeout, interval).Should(Succeed())
}

var _ = Describe("SylvaWorkloadCluster controller", func() {

	ctx := context.Background()

	Context("When setting up the test environment", func() {
		It("Creates a basic Sylva unit release template without value", func() {
			By("Creating a basic Sylva unit release template without values")
			surTemplate := &surv1alpha1.SylvaUnitsReleaseTemplate{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "default",
					Namespace: "default",
				},
				Spec: surv1alpha1.SylvaUnitsReleaseSpec{
					ClusterType: surv1alpha1.WorkloadClusterType,
					SylvaUnitsSource: &surv1alpha1.SylvaUnitsSource{
						Type: "git",
						URL:  "https://gitlab.com/sylva-projects/sylva-core.git",
						Tag:  "1.2.1",
					},
					ValuesFrom: []surv1alpha1.ValuesFrom{
						{
							LayerName: "configmap-managed",
							Name:      "managed-workload-clusters-settings",
							Type:      "ConfigMap",
							ValuesKey: "values",
						},
					},
				},
			}
			Expect(k8sClient.Create(ctx, surTemplate)).Should(Succeed())
		})

		It("Creates capo SylvaWorkloadCluster resources", func() {
			By("Creating a capo SylvaWorkloadCluster without values")
			sylvaWorkloadCluster := createSylvaWorkloadCluster()
			sylvaWorkloadCluster.Spec.ClusterSettings = apiextensionsv1.JSON{}
			sylvaWorkloadCluster.Spec.Infrastructure = swcv1alpha1.OpenstackInfrastructureProvider
			Expect(k8sClient.Create(ctx, sylvaWorkloadCluster)).To(Succeed())

			By("Getting the previously created SylvaWorkloadCluster")
			_ = getSURFromSWC(ctx, sylvaWorkloadCluster.Name, sylvaWorkloadCluster.Namespace)

			By("Removing it")
			removeSWC(ctx, sylvaWorkloadCluster)
		})

		It("Updates the basic Sylva unit release template with values and secretValues", func() {
			By("Getting the existing Sylva unit release template")
			surTemplateNamspacedName := types.NamespacedName{Name: "default", Namespace: "default"}
			createdSurt := &surv1alpha1.SylvaUnitsReleaseTemplate{}
			Eventually(func(g Gomega) {
				g.Expect(k8sClient.Get(ctx, surTemplateNamspacedName, createdSurt)).To(Succeed())
			}, timeout, interval).Should(Succeed())

			By("Updates the existing Sylva unit release template with values and secretValues")
			surtValues := map[string]any{
				"foo": "bar",
				"cluster": map[string]any{
					"capo": map[string]any{
						"network_id": "def456",
						"baz":        "qux",
					},
				},
			}
			surtValuesEncoded, _ := json.Marshal(surtValues)
			createdSurt.Spec.Values = &apiextensionsv1.JSON{
				Raw: surtValuesEncoded,
			}
			createdSurt.Spec.SecretValues = surv1alpha1.SecretValuesSpec{
				Path: "default",
			}
			Expect(k8sClient.Update(ctx, createdSurt)).Should(Succeed())
		})

		It("Creates capo SylvaWorkloadCluster resources", func() {
			By("Creating a capo SylvaWorkloadCluster")
			sylvaWorkloadCluster := createSylvaWorkloadCluster()
			sylvaWorkloadCluster.Spec.Infrastructure = swcv1alpha1.OpenstackInfrastructureProvider
			Expect(k8sClient.Create(ctx, sylvaWorkloadCluster)).To(Succeed())

			By("Getting the previously created SylvaWorkloadCluster")
			_ = getSURFromSWC(ctx, sylvaWorkloadCluster.Name, sylvaWorkloadCluster.Namespace)

			By("Removing it")
			removeSWC(ctx, sylvaWorkloadCluster)
		})

		It("Creates capd SylvaWorkloadCluster resources, ensures the related Sylva unit release exists and check that template values are correctly used", func() {
			By("Creating a basic capd SylvaWorkloadCluster")
			sylvaWorkloadCluster := createSylvaWorkloadCluster()
			Expect(k8sClient.Create(ctx, sylvaWorkloadCluster)).Should(Succeed())
			By("Getting the created sylva unit release")
			createdSylvaUnitRelease := getSURFromSWC(ctx, sylvaWorkloadCluster.Name, sylvaWorkloadCluster.Namespace)
			By("Ensuring values are correct")
			Expect(createdSylvaUnitRelease.Spec.SylvaUnitsSource.Tag).To(Equal("1.2.1"))                            // SUR Template source field is used as default
			Expect(createdSylvaUnitRelease.Spec.ValuesFrom[0].Name).To(Equal("managed-workload-clusters-settings")) // SUR Template valuesFrom is used as default
			result := &surTestValues{}
			_ = json.Unmarshal(createdSylvaUnitRelease.Spec.Values.Raw, result)
			Expect(result.K8s_version_short).To(Equal("1.29"))         // User provided SWC value is taken into account
			Expect(result.Cluster.Capo.Network_id).To(Equal("abc123")) // SUR Template values are overridden by SWC ones when existing
			Expect(result.Foo).To(Equal("bar"))                        // SUR Template values are not ignored when no SWC value exist
			Expect(result.Cluster.Capo.Baz).To(Equal("qux"))           // SUR Template values and SWC values are well merged, even deep in maps
			By("Removing it")
			removeSWC(ctx, sylvaWorkloadCluster)
		})
		It("Creates capd SylvaWorkloadCluster resources with user provided specific versions", func() {
			By("Creating a basic capd SylvaWorkloadCluster")
			sylvaWorkloadCluster := createSylvaWorkloadCluster()
			sylvaWorkloadCluster.Spec.SylvaSourceVersion = &swcv1alpha1.SylvaSourceVersion{
				Branch: "mybranch",
				Tag:    "1.3.0",
				Commit: "dummy",
			}
			Expect(k8sClient.Create(ctx, sylvaWorkloadCluster)).Should(Succeed())
			By("Getting the created sylva unit release")
			createdSylvaUnitRelease := getSURFromSWC(ctx, sylvaWorkloadCluster.Name, sylvaWorkloadCluster.Namespace)
			By("Ensuring values are correct")
			Expect(createdSylvaUnitRelease.Spec.SylvaUnitsSource.Type).To(Equal("git"))
			Expect(createdSylvaUnitRelease.Spec.SylvaUnitsSource.URL).To(Equal("https://gitlab.com/sylva-projects/sylva-core.git"))
			Expect(createdSylvaUnitRelease.Spec.SylvaUnitsSource.Branch).To(Equal("mybranch"))
			Expect(createdSylvaUnitRelease.Spec.SylvaUnitsSource.Tag).To(Equal("1.3.0"))
			Expect(createdSylvaUnitRelease.Spec.SylvaUnitsSource.Commit).To(Equal("dummy"))
			By("Removing it")
			removeSWC(ctx, sylvaWorkloadCluster)
		})
		It("Creates capd SylvaWorkloadCluster resources and ensures that secretValues are correctly merged", func() {
			By("Creating a basic capd SylvaWorkloadCluster without secretValues")
			sylvaWorkloadCluster := createSylvaWorkloadCluster()
			sylvaWorkloadCluster.Spec.SecretValues = swcv1alpha1.SecretValuesSpec{}
			Expect(k8sClient.Create(ctx, sylvaWorkloadCluster)).Should(Succeed())
			By("Getting the created sylva unit release")
			createdSylvaUnitRelease := getSURFromSWC(ctx, sylvaWorkloadCluster.Name, sylvaWorkloadCluster.Namespace)
			By("Ensuring that SUR Template secretValues are used as default")
			Expect(createdSylvaUnitRelease.Spec.SecretValues.Path).To(Equal("default")) // SUR Template values
			Expect(createdSylvaUnitRelease.Spec.SecretValues.Key).To(Equal(""))         // SUR Template values
			By("Removing it")
			removeSWC(ctx, sylvaWorkloadCluster)

			By("Creating a basic capd SylvaWorkloadCluster with secretValues")
			sylvaWorkloadCluster = createSylvaWorkloadCluster()
			Expect(k8sClient.Create(ctx, sylvaWorkloadCluster)).Should(Succeed())
			By("Getting the created sylva unit release")
			createdSylvaUnitRelease = getSURFromSWC(ctx, sylvaWorkloadCluster.Name, sylvaWorkloadCluster.Namespace)
			By("Ensuring SWC secretValues are used")
			Expect(createdSylvaUnitRelease.Spec.SecretValues.Path).To(Equal("common")) // SWC USer values
			Expect(createdSylvaUnitRelease.Spec.SecretValues.Key).To(Equal("capo"))    // SWC USer values
			By("Removing it")
			removeSWC(ctx, sylvaWorkloadCluster)

			By("Creating a basic capd SylvaWorkloadCluster with only secretValues.key")
			sylvaWorkloadCluster = createSylvaWorkloadCluster()
			sylvaWorkloadCluster.Spec.SecretValues = swcv1alpha1.SecretValuesSpec{
				Key: "capo",
			}
			Expect(k8sClient.Create(ctx, sylvaWorkloadCluster)).Should(Succeed())
			By("Getting the created sylva unit release")
			createdSylvaUnitRelease = getSURFromSWC(ctx, sylvaWorkloadCluster.Name, sylvaWorkloadCluster.Namespace)
			By("Ensuring secretValues from both the SURT and the SWC are well merged are ")
			Expect(createdSylvaUnitRelease.Spec.SecretValues.Path).To(Equal("default")) // SUR Template values
			Expect(createdSylvaUnitRelease.Spec.SecretValues.Key).To(Equal("capo"))     // SWC USer values
			By("Removing it")
			removeSWC(ctx, sylvaWorkloadCluster)
		})

		It("Creates capd SylvaWorkloadCluster resources and ensures that overridingValues values override the other ones", func() {
			By("Creating a basic capd SylvaWorkloadCluster with overriding values")
			sylvaWorkloadCluster := createSylvaWorkloadCluster()
			sylvaWorkloadCluster.Spec.OverridingValues = apiextensionsv1.JSON{
				Raw: []byte(`{"foo":"toto"}`),
			}
			Expect(k8sClient.Create(ctx, sylvaWorkloadCluster)).Should(Succeed())

			By("Getting the previously created SylvaWorkloadCluster")
			createdSylvaUnitRelease := getSURFromSWC(ctx, sylvaWorkloadCluster.Name, sylvaWorkloadCluster.Namespace)

			By("Ensuring OverridingValues are actually overriding values")
			result := &surTestValues{}
			_ = json.Unmarshal(createdSylvaUnitRelease.Spec.Values.Raw, result)
			Expect(result.Foo).To(Equal("toto"))

			By("Removing the workload cluster")
			removeSWC(ctx, sylvaWorkloadCluster)
		})

		It("Fails at creating a cluster if CRD is not respected", func() {
			By("Not creating SylvaWorkloadCluster with wrong infrastructure provider")
			sylvaWorkloadCluster := createSylvaWorkloadCluster()
			sylvaWorkloadCluster.Spec.Infrastructure = "foo"
			Expect(k8sClient.Create(ctx, sylvaWorkloadCluster)).To(Not(Succeed()))

			By("Not creating SylvaWorkloadCluster with wrong bootstrap provider")
			sylvaWorkloadCluster = createSylvaWorkloadCluster()
			sylvaWorkloadCluster.Spec.ControlPlane.Provider = "foo"
			Expect(k8sClient.Create(ctx, sylvaWorkloadCluster)).To(Not(Succeed()))

			By("Not creating SylvaWorkloadCluster with wrong Kubernetes version provider")
			sylvaWorkloadCluster = createSylvaWorkloadCluster()
			sylvaWorkloadCluster.Spec.K8sVersion = "14"
			Expect(k8sClient.Create(ctx, sylvaWorkloadCluster)).To(Not(Succeed()))

			By("Not creating SylvaWorkloadCluster without source")
			sylvaWorkloadCluster = createSylvaWorkloadCluster()
			sylvaWorkloadCluster.Spec.SylvaSourceVersion = &swcv1alpha1.SylvaSourceVersion{}
			Expect(k8sClient.Create(ctx, sylvaWorkloadCluster)).To(Not(Succeed()))

			By("Not creating SylvaWorkloadCluster with wrong values in secretValues:path")
			sylvaWorkloadCluster = createSylvaWorkloadCluster()
			sylvaWorkloadCluster.Spec.SecretValues = swcv1alpha1.SecretValuesSpec{
				Path: "common/../not_my_team",
				Key:  "secret",
			}
			err := k8sClient.Create(ctx, sylvaWorkloadCluster)
			Expect(err).To(Not(BeNil()))
			Expect(err.Error()).To(ContainSubstring("'../' is not allowed in secretValues:path"))

			By("Not creating SylvaWorkloadCluster with wrong values in secretValues:key")
			sylvaWorkloadCluster = createSylvaWorkloadCluster()
			sylvaWorkloadCluster.Spec.SecretValues = swcv1alpha1.SecretValuesSpec{
				Path: "common",
				Key:  "../../not_my_team/secret",
			}
			err = k8sClient.Create(ctx, sylvaWorkloadCluster)
			Expect(err).To(Not(BeNil()))
			Expect(err.Error()).To(ContainSubstring("'../' is not allowed in secretValues:key"))

			By("Not creating SylvaWorkloadCluster with wrong values in all secretValues fields")
			sylvaWorkloadCluster = createSylvaWorkloadCluster()
			sylvaWorkloadCluster.Spec.SecretValues = swcv1alpha1.SecretValuesSpec{
				Path: "common/../not_my_team",
				Key:  "secret../",
			}
			err = k8sClient.Create(ctx, sylvaWorkloadCluster)
			Expect(err).To(Not(BeNil()))
			Expect(err.Error()).To(ContainSubstring("'../' is not allowed in secretValues:key"))
			Expect(err.Error()).To(ContainSubstring("'../' is not allowed in secretValues:path"))
		})

		It("Fails at creating a healthy cluster if clusterSettings parameter contains invalid patterns", func() {
			By("Creating a SylvaWorkloadCluster with simple wrong cluster settings")
			sylvaWorkloadCluster := createSylvaWorkloadCluster()
			sylvaWorkloadCluster.Spec.ClusterSettings = apiextensionsv1.JSON{
				Raw: []byte(`{"baz":"{{lookup secrets}}"}`),
			}
			Expect(k8sClient.Create(ctx, sylvaWorkloadCluster)).To(Succeed())

			By("Getting the previously created SylvaWorkloadCluster")
			sylvaWorkloadClusterNamspacedName := types.NamespacedName{Name: sylvaWorkloadCluster.Name, Namespace: sylvaWorkloadCluster.Namespace}
			createdSylvaWorkloadCluster := &swcv1alpha1.SylvaWorkloadCluster{}
			Eventually(func(g Gomega) {
				g.Expect(k8sClient.Get(ctx, sylvaWorkloadClusterNamspacedName, createdSylvaWorkloadCluster)).To(Succeed())
			}, timeout, interval).Should(Succeed())

			By("Checking the conditions")
			Eventually(func(g Gomega) {
				g.Expect(k8sClient.Get(ctx, sylvaWorkloadClusterNamspacedName, createdSylvaWorkloadCluster)).To(Succeed())
				g.Expect(len(createdSylvaWorkloadCluster.Status.Conditions)).ToNot(BeZero())
				g.Expect(createdSylvaWorkloadCluster.Status.Conditions).To(ContainElement(
					MatchFields(IgnoreExtras, Fields{
						"Type":    Equal("Ready"),
						"Status":  Equal(metav1.ConditionFalse),
						"Reason":  Equal("InvalidPatternDetected"),
						"Message": Equal("spec.clusterSettings contains invalid {{..}} pattern"),
					}),
				))
			}, timeout, interval).Should(Succeed())

			By("Removing it")
			Expect(k8sClient.Delete(ctx, sylvaWorkloadCluster)).Should(Succeed())
			Eventually(func(g Gomega) {
				g.Expect(k8sClient.Get(ctx, sylvaWorkloadClusterNamspacedName, createdSylvaWorkloadCluster)).To(Not(Succeed()))
			}, timeout, interval).Should(Succeed())

			By("Creating a SylvaWorkloadCluster with complex wrong cluster settings")
			sylvaWorkloadCluster = createSylvaWorkloadCluster()
			sylvaWorkloadCluster.Spec.ClusterSettings = apiextensionsv1.JSON{
				Raw: []byte(`{"foo":{"bar":"{{lookup secrets}}"}}`),
			}
			Expect(k8sClient.Create(ctx, sylvaWorkloadCluster)).To(Succeed())

			By("Getting the previously created SylvaWorkloadCluster")
			sylvaWorkloadClusterNamspacedName = types.NamespacedName{Name: sylvaWorkloadCluster.Name, Namespace: sylvaWorkloadCluster.Namespace}
			createdSylvaWorkloadCluster = &swcv1alpha1.SylvaWorkloadCluster{}
			Eventually(func(g Gomega) {
				g.Expect(k8sClient.Get(ctx, sylvaWorkloadClusterNamspacedName, createdSylvaWorkloadCluster)).To(Succeed())
			}, timeout, interval).Should(Succeed())

			By("Checking the conditions")
			Eventually(func(g Gomega) {
				g.Expect(k8sClient.Get(ctx, sylvaWorkloadClusterNamspacedName, createdSylvaWorkloadCluster)).To(Succeed())
				g.Expect(len(createdSylvaWorkloadCluster.Status.Conditions)).ToNot(BeZero())
				g.Expect(createdSylvaWorkloadCluster.Status.Conditions).To(ContainElement(
					MatchFields(IgnoreExtras, Fields{
						"Type":    Equal("Ready"),
						"Status":  Equal(metav1.ConditionFalse),
						"Reason":  Equal("InvalidPatternDetected"),
						"Message": Equal("spec.clusterSettings contains invalid {{..}} pattern"),
					}),
				))
			}, timeout, interval).Should(Succeed())

			By("Removing it")
			Expect(k8sClient.Delete(ctx, sylvaWorkloadCluster)).Should(Succeed())
			Eventually(func(g Gomega) {
				g.Expect(k8sClient.Get(ctx, sylvaWorkloadClusterNamspacedName, createdSylvaWorkloadCluster)).To(Not(Succeed()))
			}, timeout, interval).Should(Succeed())
		})

		It("Fails at creating a healthy cluster if overridingValues parameter contains invalid patterns", func() {
			By("Creating a SylvaWorkloadCluster with overriding values including {{ ... }}")
			sylvaWorkloadCluster := createSylvaWorkloadCluster()
			sylvaWorkloadCluster.Spec.OverridingValues = apiextensionsv1.JSON{
				Raw: []byte(`{"foo":"{{lookup secrets}}"}`),
			}
			Expect(k8sClient.Create(ctx, sylvaWorkloadCluster)).To(Succeed())

			By("Getting the previously created SylvaWorkloadCluster")
			sylvaWorkloadClusterNamspacedName := types.NamespacedName{Name: sylvaWorkloadCluster.Name, Namespace: sylvaWorkloadCluster.Namespace}
			createdSylvaWorkloadCluster := &swcv1alpha1.SylvaWorkloadCluster{}
			Eventually(func(g Gomega) {
				g.Expect(k8sClient.Get(ctx, sylvaWorkloadClusterNamspacedName, createdSylvaWorkloadCluster)).To(Succeed())
			}, timeout, interval).Should(Succeed())

			By("Checking that the Ready condition is KO")
			Eventually(func(g Gomega) {
				g.Expect(k8sClient.Get(ctx, sylvaWorkloadClusterNamspacedName, createdSylvaWorkloadCluster)).To(Succeed())
				g.Expect(len(createdSylvaWorkloadCluster.Status.Conditions)).ToNot(BeZero())
				g.Expect(createdSylvaWorkloadCluster.Status.Conditions).To(ContainElement(
					MatchFields(IgnoreExtras, Fields{
						"Type":    Equal("Ready"),
						"Status":  Equal(metav1.ConditionFalse),
						"Reason":  Equal("InvalidPatternDetected"),
						"Message": Equal("spec.overridingValues contains invalid {{..}} pattern"),
					}),
				))
			}, timeout, interval).Should(Succeed())

			By("Removing it")
			Expect(k8sClient.Delete(ctx, sylvaWorkloadCluster)).Should(Succeed())
			Eventually(func(g Gomega) {
				g.Expect(k8sClient.Get(ctx, sylvaWorkloadClusterNamspacedName, createdSylvaWorkloadCluster)).To(Not(Succeed()))
			}, timeout, interval).Should(Succeed())

			By("Not creating a SylvaWorkloadCluster with overriding values including invalid key: cluster")
			sylvaWorkloadCluster = createSylvaWorkloadCluster()
			sylvaWorkloadCluster.Spec.OverridingValues = apiextensionsv1.JSON{}
			unmarshalledOverridingValues := map[string]any{
				"cluster": map[string]any{
					"capo": map[string]any{
						"network_id": "xyz987",
					},
				},
			}
			sylvaWorkloadCluster.Spec.OverridingValues.Raw, _ = json.Marshal(unmarshalledOverridingValues)
			Expect(k8sClient.Create(ctx, sylvaWorkloadCluster)).To(Not(Succeed()))

			By("Not creating a SylvaWorkloadCluster with overriding values including the invalid key: units")
			sylvaWorkloadCluster = createSylvaWorkloadCluster()
			sylvaWorkloadCluster.Spec.OverridingValues = apiextensionsv1.JSON{}
			sylvaWorkloadCluster.Spec.OverridingValues.Raw = []byte(`{"units":"foo"}`)
			Expect(k8sClient.Create(ctx, sylvaWorkloadCluster)).To(Not(Succeed()))

			By("Not creating a SylvaWorkloadCluster with overriding values including the invalid key: k8s_version_short")
			sylvaWorkloadCluster = createSylvaWorkloadCluster()
			sylvaWorkloadCluster.Spec.OverridingValues = apiextensionsv1.JSON{}
			sylvaWorkloadCluster.Spec.OverridingValues.Raw = []byte(`{"k8s_version_short":"1.29"}`)
			Expect(k8sClient.Create(ctx, sylvaWorkloadCluster)).To(Not(Succeed()))
		})
	})

	Context("When setting up the test environment where the Sylva units release CRD will be removed", func() {
		It("Removes the Sylva units release CRD", func() {
			By("Deleting the Sylva units release CRD")
			CRDName := "sylvaunitsreleases.unitsoperator.sylva"
			CRDNamspacedName := types.NamespacedName{Name: CRDName}
			CRD := &apiextensionsv1.CustomResourceDefinition{}
			Expect(k8sClient.Get(ctx, CRDNamspacedName, CRD)).To(Succeed())
			Expect(k8sClient.Delete(ctx, CRD)).To(Succeed())
		})

		It("Still can create and remove a simple SylvaWorkloadCluster resources", func() {
			By("Creating a capo SylvaWorkloadCluster")
			sylvaWorkloadCluster := createSylvaWorkloadCluster()
			sylvaWorkloadCluster.Spec.Infrastructure = swcv1alpha1.OpenstackInfrastructureProvider
			Expect(k8sClient.Create(ctx, sylvaWorkloadCluster)).To(Succeed())

			By("Getting the previously created SylvaWorkloadCluster")
			sylvaWorkloadClusterNamspacedName := types.NamespacedName{Name: sylvaWorkloadCluster.Name, Namespace: sylvaWorkloadCluster.Namespace}
			createdSylvaWorkloadCluster := &swcv1alpha1.SylvaWorkloadCluster{}
			Eventually(func(g Gomega) {
				g.Expect(k8sClient.Get(ctx, sylvaWorkloadClusterNamspacedName, createdSylvaWorkloadCluster)).To(Succeed())
			}, timeout, interval).Should(Succeed())

			By("Waiting for SylvaWorkloadCluster to have a Ready condition set")
			Eventually(func(g Gomega) {
				g.Expect(k8sClient.Get(ctx, sylvaWorkloadClusterNamspacedName, createdSylvaWorkloadCluster)).To(Succeed())
				g.Expect(createdSylvaWorkloadCluster.Status.Conditions).To(ContainElement(
					// Just as an example as we can't have a Ready SWC as long as we don't mock the SylvaUnitRelease status
					MatchFields(IgnoreExtras, Fields{"Type": Equal("Ready")})))
			}, timeout, interval).Should(Succeed())

			By("Removing it")
			Expect(k8sClient.Delete(ctx, sylvaWorkloadCluster)).Should(Succeed())
			Eventually(func(g Gomega) {
				g.Expect(k8sClient.Get(ctx, sylvaWorkloadClusterNamspacedName, createdSylvaWorkloadCluster)).To(Not(Succeed()))
			}, timeout, interval).Should(Succeed())
		})
	})

})
