/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"encoding/json"
	"fmt"
	"regexp"
	"time"

	"github.com/fluxcd/pkg/runtime/transform"
	apiextensionsv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	kerrors "k8s.io/apimachinery/pkg/util/errors"
	"k8s.io/client-go/tools/record"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	sylvav1alpha1 "gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/api/v1alpha1"
	swcv1alpha1 "gitlab.com/sylva-projects/sylva-elements/workload-cluster-operator/api/v1alpha1"

	"github.com/fluxcd/pkg/runtime/conditions"
	"github.com/fluxcd/pkg/runtime/patch"
)

const (
	resourcePollingInterval = 5 * time.Second
	fieldOwner              = "sylva-units-release-controller"
)

// SylvaWorkloadClusterReconciler reconciles a SylvaWorkloadCluster object
type SylvaWorkloadClusterReconciler struct {
	client.Client
	Scheme *runtime.Scheme
	record.EventRecorder
}

const finalizerName = "workloadclusteroperator.sylva/finalizer"

//+kubebuilder:rbac:groups=workloadclusteroperator.sylva,resources=sylvaworkloadclusters,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=workloadclusteroperator.sylva,resources=sylvaworkloadclusters/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=workloadclusteroperator.sylva,resources=sylvaworkloadclusters/finalizers,verbs=update
//+kubebuilder:rbac:groups="",resources=events,verbs=create;patch

// +kubebuilder:rbac:groups=unitsoperator.sylva,resources=sylvaunitsreleases,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=unitsoperator.sylva,resources=sylvaunitsreleasetemplates,verbs=get;list;watch

func (r *SylvaWorkloadClusterReconciler) Reconcile(ctx context.Context, req ctrl.Request) (result ctrl.Result, retErr error) {
	log := ctrl.LoggerFrom(ctx)
	log.Info("Reconcile SylvaWorkloadCluster")

	workloadCluster := &swcv1alpha1.SylvaWorkloadCluster{}
	if err := r.Get(ctx, req.NamespacedName, workloadCluster); err != nil {
		log.Error(err, "Unable to fetch SylvaWorkloadCluster resource, ignoring")
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}
	// Initialize the patch helper.
	patchHelper := patch.NewSerialPatcher(workloadCluster, r.Client)

	// Always attempt to Patch the Cluster object and status after each reconciliation.
	defer func() {
		patchOpts := []patch.Option{}
		if retErr != nil {
			conditions.MarkFalse(workloadCluster, swcv1alpha1.ReadyCondition, swcv1alpha1.ReconciliationFailedReason, "Failed to reconcile SylvaWorkloadCluster: %s", retErr)
		} else if conditions.IsTrue(workloadCluster, swcv1alpha1.ReadyCondition) && workloadCluster.Status.ObservedGeneration != workloadCluster.Generation {
			// Patch ObservedGeneration as the reconciliation completed successfully
			r.Eventf(workloadCluster, "Normal", "ReconciliationSucceeded", "Successfully reconciled SylvaWorkloadCluster %s", workloadCluster.Name)
			patchOpts = append(patchOpts, patch.WithStatusObservedGeneration{})
		}
		if err := patchHelper.Patch(ctx, workloadCluster, patchOpts...); err != nil {
			retErr = kerrors.NewAggregate([]error{retErr, err})
		}
	}()

	// Handle deletion reconciliation loop.
	if !workloadCluster.ObjectMeta.DeletionTimestamp.IsZero() {
		return r.reconcileDelete(ctx, workloadCluster)
	}

	// Add finalizer here as it can only be added when the deletionTimestamp is not set.
	if !controllerutil.ContainsFinalizer(workloadCluster, finalizerName) {
		controllerutil.AddFinalizer(workloadCluster, finalizerName)
		return ctrl.Result{Requeue: true}, nil
	}

	// Handle normal reconciliation loop.
	return r.reconcile(ctx, workloadCluster)
}

func (r *SylvaWorkloadClusterReconciler) reconcile(ctx context.Context, workloadCluster *swcv1alpha1.SylvaWorkloadCluster) (ctrl.Result, error) {

	if forbidden, reason, message := r.containsInvalidPatterns(workloadCluster); forbidden {
		// Set the condition on the workloadCluster
		conditions.MarkFalse(workloadCluster, swcv1alpha1.ReadyCondition, reason, "%s", message)
		// Update the status of the workloadCluster
		if err := r.Status().Update(ctx, workloadCluster); err != nil {
			// Handle error on status update
			return ctrl.Result{}, err
		}
		// Stop reconciliation and requeue
		return ctrl.Result{}, fmt.Errorf("%s", message)
	}

	suRelease, err := r.GenerateSylvaUnitsRelease(ctx, workloadCluster)
	if err != nil {
		return ctrl.Result{}, err
	}
	if _, err = r.applyManagedResource(ctx, suRelease); err != nil {
		return ctrl.Result{}, err
	}
	if conditions.IsReady(suRelease) {
		conditions.MarkTrue(workloadCluster, swcv1alpha1.ReadyCondition, swcv1alpha1.ReconciliationSucceededReason, "Successfully reconciled SylvaWorkloadCluster")
		return ctrl.Result{}, nil
	} else {
		conditions.MarkFalse(workloadCluster, swcv1alpha1.ReadyCondition, swcv1alpha1.ResourceNotReadyReason, "SylvaUnitsRelease resource is not Ready")
		return ctrl.Result{RequeueAfter: resourcePollingInterval}, nil
	}
}

func (r *SylvaWorkloadClusterReconciler) reconcileDelete(ctx context.Context, workloadCluster *swcv1alpha1.SylvaWorkloadCluster) (ctrl.Result, error) {
	namespacedName := types.NamespacedName{Name: workloadCluster.Name, Namespace: workloadCluster.Namespace}

	if deleted, err := r.deleteManagedResource(ctx, &sylvav1alpha1.SylvaUnitsRelease{}, namespacedName); err != nil {
		return ctrl.Result{}, err
	} else if !deleted {
		conditions.MarkFalse(workloadCluster, swcv1alpha1.ReadyCondition, swcv1alpha1.ResourcePruningReason, "SylvaUnitsRelease is being deleted")
		return ctrl.Result{RequeueAfter: resourcePollingInterval}, nil
	}
	controllerutil.RemoveFinalizer(workloadCluster, finalizerName)
	r.Eventf(workloadCluster, "Normal", "ReconciliationSucceeded", "Successfully deleted SylvaWorkloadCluster %s", workloadCluster.Name)
	return ctrl.Result{}, nil
}

func (r *SylvaWorkloadClusterReconciler) applyManagedResource(ctx context.Context, obj client.Object) (client.Object, error) {
	namespacedName := client.ObjectKeyFromObject(obj)
	objKind, err := r.Client.GroupVersionKindFor(obj)
	if err != nil {
		return nil, err
	}
	log := ctrl.LoggerFrom(ctx).WithValues("Kind", objKind.Kind, "NamespacedName", namespacedName)
	// Patch object using server-side apply strategy
	obj.GetObjectKind().SetGroupVersionKind(objKind)
	if err := r.Client.Patch(ctx, obj, client.Apply, client.ForceOwnership, client.FieldOwner(fieldOwner)); err != nil {
		return obj, fmt.Errorf("failed to patch %s %s: %w", objKind.Kind, namespacedName.String(), err)
	}
	log.Info(fmt.Sprintf("Applied desired state to %s %s", objKind.Kind, namespacedName.String()))
	return obj, nil
}

func (r *SylvaWorkloadClusterReconciler) deleteManagedResource(
	ctx context.Context,
	obj client.Object,
	namespacedName types.NamespacedName) (bool, error) {

	objKind, err := r.Client.GroupVersionKindFor(obj)
	if err != nil {
		return false, err
	}
	log := ctrl.LoggerFrom(ctx).WithValues("Kind", objKind.Kind, "NamespacedName", namespacedName)

	if err := r.Get(ctx, namespacedName, obj); err != nil {
		switch err.(type) {
		case *meta.NoKindMatchError:
			log.Info("No matches for the kind. Probably the related CRD is not installed")
			return true, nil
		default:
			if apierrors.IsNotFound(err) {
				log.Info("Successfully deleted flux object")
				return true, nil
			}
			return false, fmt.Errorf("failed to retrieve %s %s for deletion: %w", objKind.Kind, namespacedName.String(), err)
		}
	}
	if err := r.Delete(ctx, obj); err != nil {
		if apierrors.IsNotFound(err) {
			// Object has been deleted in the meantime
			return true, nil
		}
		return false, fmt.Errorf("failed to delete %s %s: %w", objKind.Kind, namespacedName.String(), err)
	}
	// Return false to requeue until managed object is actually deleted
	return false, nil
}

func (r *SylvaWorkloadClusterReconciler) GenerateSylvaUnitsRelease(ctx context.Context, workloadCluster *swcv1alpha1.SylvaWorkloadCluster) (*sylvav1alpha1.SylvaUnitsRelease, error) {
	var infraProvider string
	var bootstrapProvider string

	// Switch case to determine infra_provider based on workloadCluster.Spec.infrastructure
	switch workloadCluster.Spec.Infrastructure {
	case swcv1alpha1.OpenstackInfrastructureProvider:
		infraProvider = "capo"
	case swcv1alpha1.VsphereInfrastructureProvider:
		infraProvider = "capv"
	case swcv1alpha1.DockerInfrastructureProvider:
		infraProvider = "capd"
	case swcv1alpha1.Metal3InfrastructureProvider:
		infraProvider = "capm3"
	}

	// Switch case to determine bootstrap_provider based on workloadCluster.Spec.controlPlane.provider
	switch workloadCluster.Spec.ControlPlane.Provider {
	case swcv1alpha1.Rke2BootstrapProvider:
		bootstrapProvider = "cabpr"
	case swcv1alpha1.KubeadmBootstrapProvider:
		bootstrapProvider = "cabpk"
	}

	// Construct the machine_deployments structure
	machineDeployments := make(map[string]interface{})
	for mdName, mdSpec := range workloadCluster.Spec.MachineDeployments {
		machineDeployments[mdName] = map[string]interface{}{
			"replicas": mdSpec.Replicas,
		}
	}

	// Predefined fields for the 'cluster' key
	clusterMap := map[string]interface{}{
		"capi_providers": map[string]interface{}{
			"infra_provider":     infraProvider,
			"bootstrap_provider": bootstrapProvider,
		},
		"machine_deployments": machineDeployments,
	}

	if workloadCluster.Spec.ClusterSettings.Size() > 0 {
		clusterSpec := map[string]any{}
		// Unmarshal the JSON data into clusterSpec
		message := "Error unmarshalling clusterSettings"
		if err := json.Unmarshal(workloadCluster.Spec.ClusterSettings.Raw, &clusterSpec); err != nil {
			// Set the condition on the workloadCluster
			conditions.MarkFalse(workloadCluster, swcv1alpha1.ReadyCondition, swcv1alpha1.UnmarshalErrorReason, "%s", message)
			return nil, fmt.Errorf("%s", message)
		}
		// Use MergeMaps for a deep merge
		clusterMap = transform.MergeMaps(clusterMap, clusterSpec)
	}

	unitsData := make(map[string]interface{})
	for unit, status := range workloadCluster.Spec.EnabledUnits {
		unitsData[unit] = map[string]bool{
			"enabled": status,
		}
	}

	// Create a JSON object for values
	valuesMap := map[string]interface{}{
		"k8s_version_short": workloadCluster.Spec.K8sVersion,
		"cluster":           clusterMap, // Using the merged clusterMap
		"units":             unitsData,
	}

	// Set number of control plane nodes
	if workloadCluster.Spec.ControlPlane.Replicas != nil {
		valuesMap["cluster"].(map[string]interface{})["control_plane_replicas"] = workloadCluster.Spec.ControlPlane.Replicas
	}

	if workloadCluster.Spec.OverridingValues.Size() > 0 {
		overridingValues := map[string]any{}
		// Unmarshal the JSON data into values
		message := "Error unmarshalling overriding values"
		if err := json.Unmarshal(workloadCluster.Spec.OverridingValues.Raw, &overridingValues); err != nil {
			// Set the condition on the workloadCluster
			conditions.MarkFalse(workloadCluster, swcv1alpha1.ReadyCondition, swcv1alpha1.UnmarshalErrorReason, "%s", message)
			return nil, fmt.Errorf("%s", message)
		}
		// Use MergeMaps for a deep merge
		valuesMap = transform.MergeMaps(valuesMap, overridingValues) // overridingValues override values
	}

	// Merge the sylvaUnitsRelease with 'default' SylvaUnitsReleaseTemplate
	suReleaseSpec, err := r.getSylvaUnitsSpecFromTemplate(ctx, &types.NamespacedName{
		Namespace: workloadCluster.Namespace,
		Name:      "default",
	})
	if err != nil {
		return nil, err
	}

	surValues := map[string]any{}
	if suReleaseSpec.Values.Size() > 0 {
		if err := json.Unmarshal(suReleaseSpec.Values.Raw, &surValues); err != nil {
			// Unmarshal the JSON data into values
			message := "Error unmarshalling SylvaUnitsReleaseTemplate values"
			// Set the condition on the workloadCluster
			conditions.MarkFalse(workloadCluster, swcv1alpha1.ReadyCondition, swcv1alpha1.UnmarshalErrorReason, "%s", message)
			return nil, fmt.Errorf("%s", message)
		}
	} else {
		suReleaseSpec.Values = &apiextensionsv1.JSON{}
	}
	// Use MergeMaps for a deep merge
	surValues = transform.MergeMaps(surValues, valuesMap) // user values override SylvaUnitsReleaseTemplate ones

	// Convert valuesMap to a JSON RawMessage
	valuesRaw, err := json.Marshal(surValues)
	if err != nil {
		return nil, err
	}

	// Fill the rest of the SylvaUnitsRelease
	suReleaseSpec.Values.Raw = valuesRaw
	suReleaseSpec.ClusterType = sylvav1alpha1.WorkloadClusterType
	// Merge SWC secretValues with SURT secretValues. SWC ones have more priority
	switch {
	case suReleaseSpec.SecretValues == sylvav1alpha1.SecretValuesSpec{}:
		suReleaseSpec.SecretValues = sylvav1alpha1.SecretValuesSpec{
			Path: workloadCluster.Spec.SecretValues.Path,
			Key:  workloadCluster.Spec.SecretValues.Key,
		}
	case workloadCluster.Spec.SecretValues != swcv1alpha1.SecretValuesSpec{}:
		if workloadCluster.Spec.SecretValues.Path != "" {
			suReleaseSpec.SecretValues.Path = workloadCluster.Spec.SecretValues.Path
		}
		if workloadCluster.Spec.SecretValues.Key != "" {
			suReleaseSpec.SecretValues.Key = workloadCluster.Spec.SecretValues.Key
		}
	}
	// Merge SylvaUnitsSource
	if workloadCluster.Spec.SylvaSourceVersion.Branch != "" {
		suReleaseSpec.SylvaUnitsSource.Branch = workloadCluster.Spec.SylvaSourceVersion.Branch
	}
	if workloadCluster.Spec.SylvaSourceVersion.Tag != "" {
		suReleaseSpec.SylvaUnitsSource.Tag = workloadCluster.Spec.SylvaSourceVersion.Tag
	}
	if workloadCluster.Spec.SylvaSourceVersion.Commit != "" {
		suReleaseSpec.SylvaUnitsSource.Commit = workloadCluster.Spec.SylvaSourceVersion.Commit
	}

	suRelease := &sylvav1alpha1.SylvaUnitsRelease{
		ObjectMeta: metav1.ObjectMeta{
			Name:      workloadCluster.Name,
			Namespace: workloadCluster.Namespace,
		},
		Spec: *suReleaseSpec,
	}
	return suRelease, nil
}

func (r *SylvaWorkloadClusterReconciler) getSylvaUnitsSpecFromTemplate(ctx context.Context, sylvaUnitsReleaseTemplateNsName *types.NamespacedName) (*sylvav1alpha1.SylvaUnitsReleaseSpec, error) {
	log := ctrl.LoggerFrom(ctx)

	template := &sylvav1alpha1.SylvaUnitsReleaseTemplate{}
	if err := r.Get(ctx, *sylvaUnitsReleaseTemplateNsName, template); err != nil {
		return nil, fmt.Errorf("failed to retrieve SylvaUnitsReleaseTemplate %s: %w", sylvaUnitsReleaseTemplateNsName.String(), err)
	}
	log.Info("Retrieved SylvaUnitsReleaseTemplate", "template", sylvaUnitsReleaseTemplateNsName)
	return &template.Spec, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *SylvaWorkloadClusterReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&swcv1alpha1.SylvaWorkloadCluster{}).
		Complete(r)
}

func (r *SylvaWorkloadClusterReconciler) containsInvalidPatterns(workloadCluster *swcv1alpha1.SylvaWorkloadCluster) (bool, string, string) {
	// Check for `{{.*}}` pattern in several fields
	pattern := `(.|\n)*\{\{(.|\n)*\}\}(.|\n)*`
	re := regexp.MustCompile(pattern)
	if re.MatchString(string(workloadCluster.Spec.ClusterSettings.Raw)) {
		return true, swcv1alpha1.InvalidPatternDetectedReason, "spec.clusterSettings contains invalid {{..}} pattern"
	}
	if re.MatchString(string(workloadCluster.Spec.OverridingValues.Raw)) {
		return true, swcv1alpha1.InvalidPatternDetectedReason, "spec.overridingValues contains invalid {{..}} pattern"
	}
	return false, "", ""
}
